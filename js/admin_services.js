$(function () {
    // Gets Sub Title Options
    var form_data = {};
    $("#send-data").click(function () {
        // Gets Header Options
        var sub_title = [];
        $('.sub_title').each(function () {
            var section = {};
            section.title = $(this).find('#title').val();
            section.para = $(this).find('#para').val();
            section.icon = $(this).find('#icon').val();
            sub_title.push(section);
        });
        form_data.services_options = sub_title;

        $('<input />').attr('type', 'hidden').attr('name', "form_data").attr('value', JSON.stringify(form_data)).appendTo('#home-form');
        $('#home-form').submit();
    });
});