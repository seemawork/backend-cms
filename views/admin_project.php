<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Paella Intelligence: Edit Home Page</title>
        <!-- Bootstrap minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <!-- Font Awesome CSS -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
        <!-- Roboto FONT -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic" rel="stylesheet" type="text/css">
        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Bootstrap minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <!-- Moment JS (dateformatter) -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
        <!-- Template CSS -->
        <link rel="stylesheet" href="<?php echo $path . '/css/admin.css';?>">
        <!-- JS -->
        <script src="<?php echo $path . '/js/admin_delete.js';?>"></script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>

    <body>

        <!-- Login Form START -->
        <section id="admin-main">
            <div class="container">
                <div class="row">

                  <!-- Navbar -->
                  <?php $project_active = "active"; ?>
                  <?php include 'partials/nav.php';?>

                    <div class="col-sm-9 section-header">

                        <h1>Projects</h1>

                        <?php if (isset($error)) { ?>
                        <div id="error">
                            <h3><?php echo $error;?></h3>
                        </div>


                        <?php } else { ?>
                        <div class="list">
                            <p><a href="<?php echo $path . '/project/add';?>"><i class="fa fa-plus" aria-hidden="true"></i> Add Project</a></p>

                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Action</th>
                                        <th>Project Duration</th>
                                        <th>Project's Client</th>
                                        <th>Project Title</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php for($i = 0; $i < count($projects); $i++ ) { ?>
                                  <?php $item = $projects[$i] ?>
                                  <tr>
                                      <td class="post-actions">
                                        <ul>
                                          <li><a href="#" class="delete" data-title="<?php echo htmlspecialchars( $item->title); ?>"  data-link="<?php echo $path . '/project/delete/' . $item->getObjectId(); ?>"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a></li>
                                          <li><a href="<?php echo $path . '/project/edit/' . $item->getObjectId();?>"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a></li>
                                        </ul>
                                      </td>
                                      <td><?php echo htmlspecialchars($item->duration); ?></td>
                                      <td><?php echo htmlspecialchars($item->client); ?></td>
                                      <td><?php echo htmlspecialchars($item->title); ?></td>
                                  </tr>

                                  <?php } ?>

                                </tbody>
                            </table>

                            <!-- DELETE PROJECT FORM -->
                            <form id="form" method="POST" action="<?php echo $path . '/project' ?>">
                              <button type="button" value="submit" >Send Data</button>
                            </form>

                        </div>
                        <?php } ?>

                    </div>
                </div>
                <!-- ./row -->
            </div>
            <!-- /.container -->
        </section>

    </body>

</html>
