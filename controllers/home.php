<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Paella Intelligence | Software Development and A.I. Integrations</title>
        <meta name="description" content="We are a bespoke software development company that create, support and integrate artificial intelligence into systems and apps for businesses" />
        <!-- OGP Meta Tags -->
        <meta property="og:title" content="Paella Intelligence | Software Development and A.I. Integrations" />
        <meta property="og:description" content="Paella Intelligence are a bespoke software development company based in Bermondsey, London that create, support and integrate artificial intelligence into systems and apps for businesses." />
        <meta property="og:url" content="https://www.paellaintelligence.com" />
        <meta property="og:image" content="https://www.paellaintelligence.com/images/logo-large-inverted.png" />
        <meta property="og:locale" content="en_GB" />
        <!-- Favicon -->
        <link rel="icon" type="image/png" href="/images/logo-small-inverted.png">

        <!-- Bootstrap minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <!-- Font Awesome CSS -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
        <!-- Roboto FONT -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic,900,900italic" rel="stylesheet" type="text/css">
        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Bootstrap minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <!-- Home Page JavaScript -->
        <script src="/js/home.js"></script>
        <!-- Animations CSS -->
        <link rel="stylesheet" href="/css/animation.css">
        <!-- Template CSS -->
        <link rel="stylesheet" href="/css/template.css">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
        <!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
        <script src="js/consent.js" type="text/javascript"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.10/cookieconsent.min.js"></script>
        <!-- End Cookie Consent plugin -->

    </head>

    <body>
        <!-- Header Section START -->
        <header id="page-header">
            <!-- Background Image -->
            <!-- NAVBAR START -->
            <nav class="navbar navbar-default">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#paella-navbar-collapse" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                        <div class="navbar-left navbar-center">
                            <a href="/"> <img class="navbar-logo hidden-xs" src="/images/logo-text.png"> </a>
                            <a href="/"> <img class="navbar-logo visible-xs" src="/images/logo-small-inverted.png"> </a>
                        </div>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="paella-navbar-collapse">
                        <ul class="nav navbar-nav navbar-right" id="paella-navbar">
                            <li><a class="active" href="/">Home</a></li>
                            <li><a href="/services">Services</a></li>
                            <li><a href="/projects">Projects</a></li>
                            <li><a href="/about-us">About Us</a></li>
                            <li><a href="/blog">Blog</a></li>
                            <li><a href="/contact-us">Contact Us</a></li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container -->
            </nav>
            <!-- NAVBAR END -->
            <div id="header-text">
                <h1>Welcome to Paella Intelligence</h1>
                <h2>eat. sleep. create. repeat.</h2> </div>
            <a id="header-scroll" href="#services-section" class="visible-md visible-lg"> <img class="img-responsive" src="/images/hero-btn.png" /> </a>
        </header>
        <!-- Header Section END -->
        <!-- Services START -->
        <section class="index-services" id="services-section">
            <div class="container">
                <!-- Services Header -->
                <div class="row">
                    <div class="col-xs-12 section-header">
                        <h1>Services</h1> </div>
                </div>
                <!-- /.row -->
                <!-- Service Content Container .row -->
                <div class="row ">
                    <?php $services_options = $options->get('services_options'); ?>
                        <?php for($i = 0 ; $i < 3; ++$i) { ?>

                            <!-- Services Row -->
                            <div class="col-md-4">
                                <div class="row">
                                    <!-- Icon -->
                                    <?php $icon = (!empty($services_options[$i]->icon) ? $services_options[$i]->icon : 'fa-briefcase'); ?>
                                    <div class="col-sm-4  col-md-12">
                                        <div class="services-icon"> <span class="fa <?php echo $icon; ?>" aria-hidden="true"></span> </div>
                                    </div>
                                    <!-- Text -->
                                    <?php $title = (!empty($services_options[$i]->title) ? $services_options[$i]->title : 'Service Title'); ?>
                                    <?php $paragraph = (!empty($services_options[$i]->paragraph) ? $services_options[$i]->paragraph : 'Paragraph Explaining the service'); ?>
                                    <div class="col-sm-8  col-md-12 services-info">
                                        <h2><?php echo $title; ?></h2>
                                        <p> <?php echo $paragraph; ?></p>
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>

                    <?php } // For Loop END ?>

                </div>
                <!-- Service Content Container .row -->
            </div>
            <!-- /.container -->
            <div class="round-btn-format"> <a href="/services" class="btn btn-primary secondary" role="button">More Services</a></div>
        </section>
        <!-- Services END -->

        <?php /*
        <!--Testimonials START -->
        <section class="testimonials">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h1>Testimonials</h1> </div>
                </div>
                <div id="testimonials-carousel" class="carousel slide">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#testimonials-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#testimonials-carousel" data-slide-to="1"></li>
                        <li data-target="#testimonials-carousel" data-slide-to="2"></li>
                    </ol>
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        <?php $testimonials = $options->get('testimonials'); ?>
                        <?php for($i = 0 ; $i < 3; ++$i) { ?>
                            <?php $active = ($i == 0) ? "active" : ""; ?>
                            <?php $quote = (!empty($testimonials[$i]->quote) ? $testimonials[$i]->quote : 'good morning Sometimes'); ?>
                            <?php $person = (!empty($testimonials[$i]->person) ? $testimonials[$i]->person : 'client'); ?>

                            <div class="<?php echo $active;?> item">
                                <div class="Carousel-caption">
                                    <p>&quot;<?php echo $quote; ?>&quot;</p>
                                    <h3><?php echo $person ; ?></h3> </div>
                                </div>
                        <?php } // For Loop END ?>
                    </div>
                  </div>
                  <!-- Carousel nav -->
                </div>
            </div>
        </section>
        <!--Testimonials END -->
        */ ?>
        <!-- Statistics START -->
        <section id="stats-section">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="section-header">Statistics</h1> </div>
                </div>
                <div class="row stats-container">
                  <?php $statistics = $options->get('statistics');?>
                  <?php for($i = 0,$data_delay=0 ; $i < 4; ++$i,$data_delay+=400) { ?>
                      <?php $number = (!empty($statistics[$i]->number) ? $statistics[$i]->number : '0'); ?>
                      <?php $stat = (!empty($statistics[$i]->stat) ? $statistics[$i]->stat : 'STATISTIC MISSING'); ?>


                      <!-- Statistic -->
                      <div class="col-xs-12 col-sm-6 col-md-3">
                          <div class="stat-container">
                              <div class="figure">
                                  <h3 data-count="<?php echo $number; ?>"><?php echo $number; ?></h3>
                                  <p><?php echo $stat;?></p>
                              </div>
                              <svg class="circle-wrapper" viewbox="0 0 1000 1000">
                                  <circle class="circle" cx="500" cy="500" r="450" data-run="false" data-delay="<?php echo $data_delay ; ?>" />
                              </svg>
                          </div>
                      </div>
                  <?php } // For Loop END ?>

                </div>
            </div>
        </section>
        <!-- Statistics END -->
        <!-- Contact us Section Start-->
        <section class="contact-us">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h3>Contact us! You dream it and we make it a reality...</h3> </div>
                </div>
                <div class="round-btn-format"> <a href="/contact-us" class="btn btn-primary " role="button">Get in Touch</a></div>
            </div>
        </section>
        <!--Contact us Section finish-->
        <!-- Footer START -->
        <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="footer-nav">
                            <li class="hide" id="linkedin-icon"><a href="https://www.linkedin.com/company/paella-intelligence-ltd" target="_blank"><i class="fa fa-linkedin fa-lg" aria-hidden="true"></i>  </a></li>
                            <li id="twitter-icon"><a href="https://www.twitter.com/paellaintel_ltd" target="_blank"><i class="fa fa-twitter fa-lg" aria-hidden="true"></i>  </a></li>
                            <li id="facebook-icon"><a href="https://www.facebook.com/paella.intelligence" target="_blank"><i class="fa fa-facebook fa-lg" aria-hidden="true"></i> </a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12">
                        <ul class="footer-nav">
                            <li><a href="/">Home</a> </li>
                            <li><a href="/services">Services</a> </li>
                            <li><a href="/projects">Projects</a> </li>
                            <li><a href="/about-us">About Us</a> </li>
                            <li><a href="/blog">Blog</a> </li>
                            <li><a href="/contact-us">Contact Us</a></li>
                        </ul>
                        <ul class="footer-nav smaller-text">
                            <li><a href="/terms-and-conditions" target="_blank">Terms and Conditions</a> </li>
                            <li><a href="/cookie-policy" target="_blank">Cookie Policy</a> </li>
                            <li><a href="/privacy-policy" target="_blank">Privacy Policy</a> </li>
                        </ul>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </footer>
        <!-- Footer END -->
        <!-- Copyright START -->
        <footer id="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <p>Copyright &copy; Paella Intelligence LTD.</p>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </footer>
        <!-- Copyright END -->
        <!-- Google Analytics -->
        <script src="/js/analytics.js"></script>
    </body>

</html>
