<?php

use Parse\ParseQuery;
use Parse\ParseException;

class RouteController
{
    // Updates Data for the home page
    public static function updateHome()
    {
        $options =Flight::get('HomePageID');
        $data = Flight::request()->data->form_data;

        $query = new ParseQuery('HomePage');
        try {
            $home_options = $query->get($options);
            $form_data = json_decode($data);

            self::setOptions($home_options, $form_data, 'services_options', array('icon', 'title', 'paragraph'));
            self::setOptions($home_options, $form_data, 'testimonials', array('person', 'quote'));
            self::setOptions($home_options, $form_data, 'statistics', array('number', 'stat'));
       self::generateHtml($home_options, 'home.php', 'index.html');
            Flight::redirect('/');
        } catch (ParseException $ex) {
            return Flight::handleParseError($ex, 'admin_home.php');
        }
    }


    // Updates Data for the Services page
    public static function updateServices()
    {
        $options = Flight::get('ServicesPageID');
        $data = Flight::request()->data->form_data;

        $query = new ParseQuery('ServicesPage');
        try {
            $services_options = $query->get($options);
            $form_data = json_decode($data);

            self::setOptions($services_options, $form_data, 'services_options', array('title', 'para','icon'));
             self::generateHtml($services_options, 'services.php','services.html');
            Flight::redirect('/services');
        } catch (ParseException $ex) {
            return Flight::handleParseError($ex, 'admin_services.php');
        }
    }


    // Set options that define a section of the home page
    private static function setOptions($parse_object, $data, $section_name, $options)
    {
        $purifier = Flight::get('purifier');

        if (!empty($data->$section_name)) {
            $section = $data->$section_name;
            $new_options = array();

            foreach ($section as $sec) {
                foreach ($options as $item) {
                    $new_option[$item] = $purifier->purify($sec->$item);
                }
                array_push($new_options, (object) $new_option);
            }

            $parse_object->setArray($section_name, $new_options);
            try {
              $parse_object->save();
            } catch (ParseException $ex) {
                throw $ex;
            }
        }
    }

    private static function generateHtml($parse_object, $template_file, $html_file)
    {
        ob_start(); // start output buffer
        $options = $parse_object;
        include $template_file;
        $template = ob_get_contents(); // get contents of buffer
        ob_end_clean();
        file_put_contents( Flight::get('frontend_path') . $html_file, $template);
    }
}
