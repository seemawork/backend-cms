<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Blog | Paella Intelligence</title>
        <?php $description = (!empty($options->description) ? $options->description : 'missing description'); ?>
        <?php $smalldescription = substr($description, 0, 100); ?>

        <meta name="description" content="<?php echo $smalldescription; ?>" />
        <!-- OGP Meta Tags -->
        <meta property="og:title" content="Blog Post | Paella Intelligence" />
        <meta property="og:description" content="<?php echo $smalldescription; ?>" />
        <meta property="og:url" content="https://www.paellaintelligence.com/blog" />
        <meta property="og:image" content="https://www.paellaintelligence.com/images/logo-large-inverted.png" />
        <meta property="og:locale" content="en_GB" />
        <!-- Favicon -->
        <link rel="icon" type="image/png" href="/images/logo-small-inverted.png">

        <!-- Bootstrap minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <!-- Font Awesome CSS -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
        <!-- Roboto FONT -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic,900,900italic" rel="stylesheet" type="text/css">
        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Bootstrap minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <!-- Moment JS (dateformatter) -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
        <!-- Blog JS -->
        <script src="/js/blogmore.js"></script>
        <!-- Animations CSS -->
        <link rel="stylesheet" href="/css/animation.css">
        <!-- Template CSS -->
        <link rel="stylesheet" href="/css/template.css">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

        <!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
        <script src="/js/consent.js" type="text/javascript"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.10/cookieconsent.min.js"></script>
        <!-- End Cookie Consent plugin -->
    </head>

    <body>
        <!-- Header Section START -->
        <header id="page-header" class="secondary">
            <!-- Background Image -->

            <!-- NAVBAR START -->
            <nav class="navbar navbar-default">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#paella-navbar-collapse" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="navbar-left navbar-center">
                            <a href="/"> <img class="navbar-logo hidden-xs" src="/images/logo-text.png"> </a>
                            <a href="/"> <img class="navbar-logo visible-xs" src="/images/logo-small-inverted.png"> </a>
                        </div>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="paella-navbar-collapse">
                        <ul class="nav navbar-nav navbar-right" id="paella-navbar">
                            <li><a href="/">Home</a></li>
                            <li><a href="/services">Services</a></li>
                            <li><a href="/projects">Projects</a></li>
                            <li><a href="/about-us">About Us</a></li>
                            <li><a class="active" href="/blog">Blog</a></li>
                            <li><a href="/contact-us">Contact Us</a></li>

                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container -->
            </nav>
            <!-- NAVBAR END -->

            <div id="header-text">
                <h1>Blog</h1>
            </div>

        </header>
        <!-- Header Section END -->

        <!-- Main blog section started-->
        <section class="mainBlog">
          <?php $title = (!empty($options->title) ? $options->title : 'missing title'); ?>
          <?php $image = (!empty($options->image) ? $options->image : '/images/placeholder.png'); ?>
          <?php $date = (!empty($options->date) ? $options->publish_date : date('d F Y') ); ?>
          <?php $name = (!empty($options->name) ? $options->name : 'missing name'); ?>
          <?php $description = (!empty($options->description) ? $options->description : 'missing description'); ?>

            <div class="container" id="blog-container">
                <!-- Blog Post START -->
                <div class="row blog-wrapper post">
                    <div class="col-xs-12">
                        <h1 class="title"><?php echo $title; ?></h1>
                    </div>

                    <div class="col-xs-12 col-sm-8">
                        <img class="img-responsive image" src="<?php echo $image; ?>" onerror="this.src = '/images/placeholder.png'" />
                        <p class="info">
                            <span class="name"><?php echo $name; ?></span>
                            <span class="date"><?php echo $date; ?></span>
                        </p>
                        <p class="description">
                            <?php echo nl2br($description); ?>
                        </p>
                        <div class="lg-margin-bottom"></div>
                        <div data-link="<?php echo $options->getObjectId(); ?>" class="_id hide"></div>
                    </div>

                    <div class="col-xs-12 col-sm-4">
                        <div class="row">

                            <div class="col-xs-12">
                                <h1 class="recent">Recent blogs</h1>
                            </div>

                            <div class="side-post col-xs-12">

                                <h1 class="title">Our Newest Clients; Wuhan Xingye</h1>
                                <img class="img-responsive image hidden-xs" src="/images/placeholder.png" onerror="this.src = '/images/placeholder.png'" />
                                <p class="description">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mattis tempor elit eu ornare. In vel augue rutrum, elementum ex sit amet, tempor arcu. Donec in molestie metus. Suspendisse vehicula congue condimentum. Nullam nunc metus, imperdiet id ante
                                    non, pharetra iaculis odio. Phasellus at elit vel urna consequat faucibus nec commodo urna.
                                </p>
                                <div class="round-btn-format"> <a href="#" class="btn secondary btn-primary link" role="button">More</a> </div>
                                <div class="line hidden-xs">
                                    <hr> </div>
                            </div>

                            <div class="side-post col-xs-12">

                                <h1 class="title">Our Newest Clients; Wuhan Xingye</h1>
                                <img class="img-responsive image hidden-xs" src="/images/placeholder.png" onerror="this.src = '/images/placeholder.png'" />
                                <p class="description">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mattis tempor elit eu ornare. In vel augue rutrum, elementum ex sit amet, tempor arcu. Donec in molestie metus. Suspendisse vehicula congue condimentum. Nullam nunc metus, imperdiet id ante
                                    non, pharetra iaculis odio. Phasellus at elit vel urna consequat faucibus nec commodo urna.
                                </p>
                                <div class="round-btn-format"> <a href="#" class="btn secondary btn-primary link" role="button">More</a> </div>
                            </div>


                        </div>

                    </div>
                </div>
                <!-- Blog Post END -->
            </div>
        </section>
        <!-- Main blog section finish-->

        <!-- Contact us Section Start-->
        <section class="contact-us">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h3>Contact us! You dream it and we make it a reality...</h3> </div>
                </div>
                <div class="round-btn-format"> <a href="/contact-us" class="btn btn-primary " role="button">Get in Touch</a></div>
            </div>
        </section>
        <!--Contact us Section finish-->
        <!-- Footer START -->
        <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="footer-nav">
                            <li class="hide" id="linkedin-icon"><a href="https://www.linkedin.com/company/paella-intelligence-ltd" target="_blank"><i class="fa fa-linkedin fa-lg" aria-hidden="true"></i>  </a></li>
                            <li id="twitter-icon"><a href="https://www.twitter.com/paellaintel_ltd" target="_blank"><i class="fa fa-twitter fa-lg" aria-hidden="true"></i>  </a></li>
                            <li id="facebook-icon"><a href="https://www.facebook.com/paella.intelligence" target="_blank"><i class="fa fa-facebook fa-lg" aria-hidden="true"></i> </a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12">
                        <ul class="footer-nav">
                            <li><a href="/">Home</a> </li>
                            <li><a href="/services">Services</a> </li>
                            <li><a href="/projects">Projects</a> </li>
                            <li><a href="/about-us">About Us</a> </li>
                            <li><a href="/blog">Blog</a> </li>
                            <li><a href="/contact-us">Contact Us</a></li>
                        </ul>
                        <ul class="footer-nav smaller-text">
                            <li><a href="/terms-and-conditions" target="_blank">Terms and Conditions</a> </li>
                            <li><a href="/cookie-policy" target="_blank">Cookie Policy</a> </li>
                            <li><a href="/privacy-policy" target="_blank">Privacy Policy</a> </li>
                        </ul>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </footer>
        <!-- Footer END -->
        <!-- Copyright START -->
        <footer id="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <p>Copyright &copy; Paella Intelligence LTD.</p>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </footer>
        <!-- Copyright END -->

        <!-- Google Analytics -->
        <script src="/js/analytics.js"></script>
    </body>

</html>
