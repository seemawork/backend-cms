<?php

use Parse\ParseUser;
use Parse\ParseException;

class LoginController
{
    // Logs a user in
    public static function login()
    {
        $error_message = 'Invalid username/password entered';
        $email = Flight::request()->data->email;
        $password = Flight::request()->data->password;

        try {
            ParseUser::logIn($email, $password);

            return Flight::redirect('/');
        } catch (ParseException $ex) {
            return Flight::render('admin_login.php', array('error' => $error_message, 'path' => Flight::get('path') ));
        }
    }

    // Logs a user out
    public static function logout()
    {
        ParseUser::logOut();
        return Flight::redirect('/login');
    }
}
