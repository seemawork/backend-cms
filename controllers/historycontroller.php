<?php

use Parse\ParseObject;
use Parse\ParseUser;
use Parse\ParseException;
use Parse\ParseQuery;

class HistoryController
{
    // Deletes a history item with the specified id
    public static function delete($id)
    {
        $query = new ParseQuery('History');
        try {
            $history = $query->get($id);
            $history->destroy();
            return Flight::redirect('/history');
        } catch (ParseException $ex) {
            return self::handleError($ex);
        }
    }

    // Edits the history item with the specified id
    public static function edit($id)
    {
        $purifier = Flight::get('purifier');

        $history_content = !empty(Flight::request()->data->item_content) ? Flight::request()->data->item_content : "{missing-content}";
        $history_icon = !empty(Flight::request()->data->item_icon) ? Flight::request()->data->item_icon : 'fa-flag';
        $history_date = !empty(Flight::request()->data->item_date) ? Flight::request()->data->item_date :  date('d F Y');
        $query = new ParseQuery('History');
        try {
            $history = $query->get($id);
            $history->set('content', $purifier->purify($history_content) );
            $history->set('icon', $purifier->purify($history_icon) );
            $history->set('date', $purifier->purify($history_date) );
            $history->save();
            return Flight::redirect('/history');
        } catch (ParseException $ex) {
            return self::handleError($ex);
        }
    }

    // Add a new history item
    public static function add()
    {
        $purifier = Flight::get('purifier');

        $history_content = !empty(Flight::request()->data->item_content) ? Flight::request()->data->item_content : "{missing-content}";
        $history_icon = !empty(Flight::request()->data->item_icon) ? Flight::request()->data->item_icon : 'fa-flag';
        $history_date = !empty(Flight::request()->data->item_date) ? Flight::request()->data->item_date :  date('d F Y');
        try {
            $history = new ParseObject("History");
            $history->set('content', $purifier->purify($history_content) );
            $history->set('icon', $purifier->purify($history_icon) );
            $history->set('date', $purifier->purify($history_date) );
            $history->save();
            return Flight::redirect('/history');
        } catch (ParseException $ex) {
            return self::handleError($ex);
        }
    }

    /*
     * Methods used to render history related pages
     */
     public static function renderHomePage() {
         $query = new ParseQuery('History');
         $current_user = ParseUser::getCurrentUser();

         if($current_user) {
           try {
               $history = $query->find();
               return Flight::render('admin_history.php', array('current_user' => ParseUser::getCurrentUser(), 'path' => Flight::get('path'), 'history' => $history));
           } catch (ParseException $ex) {
                return Flight::handleParseError($ex, 'admin_history.php');
           }

         // User not logged in so redirect to login page
         } else {
           return Flight::redirect('/login');
         }
     }

    // Renders the page used to edit a history item
    public static function renderEditPage($id)
    {
        $query = new ParseQuery('History');
        $current_user = ParseUser::getCurrentUser();

        if($current_user) {
          try {
              $item = $query->get($id);
              return Flight::render('admin_history_edit.php', array('current_user' => ParseUser::getCurrentUser(), 'path' => Flight::get('path'), 'item' => $item));
          } catch (ParseException $ex) {
              return self::handleError($ex);
          }

        // User not logged in so redirect to login page
        } else {
          return Flight::redirect('/login');
        }
    }

    // Renders the page used to add a new history item
    public static function renderAddPage()
    {
        $current_user = ParseUser::getCurrentUser();

        if($current_user) {
          return Flight::render('admin_history_add.php', array('current_user' => ParseUser::getCurrentUser(), 'path' => Flight::get('path') ));

        // User not logged in so redirect to login page
        } else {
          return Flight::redirect('/login');
        }
    }

    // Custom parse error handler for the history page
    public static function handleError($ex)
    {
      $code = $ex->getCode();

      // Invalid Session
      if($code === 209) {
          ParseUser::logOut();
          Flight::redirect('/login');
      }

      $current_user = ParseUser::getCurrentUser();
      if($current_user) {
          Flight::redirect('/history');
      } else {
          Flight::redirect('/login');
      }
    }

}
