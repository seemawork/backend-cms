<div class="col-sm-3 admin-nav list-group">
    <p class="list-group-item" id="user">
        <?php echo htmlspecialchars($current_user->username); ?>
    </p>
    <a class="list-group-item <?php if( !empty($home_active)  ){ echo $home_active; }  ?>" href="<?php echo $path . '/';?>">Home</a>
    <a class="list-group-item <?php if( !empty($services_active)  ){ echo $services_active; }  ?>" href="<?php echo $path . '/services';?>">Services</a>
    <a class="list-group-item <?php if( !empty($project_active)  ){ echo $project_active; }  ?>" href="<?php echo $path . '/project';?>">Project</a>
    <a class="list-group-item <?php if( !empty($history_active)  ){ echo $history_active; }  ?>" href="<?php echo $path . '/history';?>">History</a>
    <a class="list-group-item <?php if( !empty($blog_active)  ){ echo $blog_active; }  ?>" href="<?php echo $path . '/blog';?>">Blog</a>
    <a class="list-group-item" href="<?php echo $path . '/logout';?>">Logout</a>
</div>
