<?php

require 'flight/Flight.php';
require 'parse-sdk/autoload.php';
require_once 'htmlpurifier-4.7.0-lite/library/HTMLPurifier.auto.php';
require 'controllers/logincontroller.php';
require 'controllers/routecontroller.php';
require 'controllers/projectcontroller.php';
require 'controllers/historycontroller.php';
require 'controllers/blogcontroller.php';

// Parse Setup
use Parse\ParseClient;
use Parse\ParseException;
use Parse\ParseUser;
use Parse\ParseQuery;
use Parse\ParseSessionStorage;

session_start();

/* PaellaTest App */
ParseClient::initialize('DqASTLWjUVX991OcCse60944QtGLLs5J0Q9PyU7e', 'Y9sfBXIa3CeTzPZuGKAki5BOVSLbuwCLWRkuwnwo', 'ApRnjiHQq4bPiTGI99xiFImZvb52Rnxexi8hHJCc');
ParseClient::setStorage(new ParseSessionStorage());

// Route Controller
Flight::set('RouteController', new RouteController());
Flight::set('BlogController', new BlogController());
Flight::set('HistoryController', new HistoryController());
Flight::set('ProjectController', new ProjectController());

/* Paella Server ID's */
Flight::set('HomePageID', '4Xn0MCwcWh');
Flight::set('ServicesPageID', '8hdLziDB8u');

/* HTML Purifier Setup */
$config = HTMLPurifier_Config::createDefault();
Flight::set('purifier', new HTMLPurifier($config));

/* Absolute and Frontend Path Setup */
setPaths();

/* ROUTES */
/*
----------------------------------------------
HOME Page Routes
----------------------------------------------
*/
Flight::route('GET /', function () {
    return renderPage('HomePage', Flight::get('HomePageID'), 'admin_home.php');
});
Flight::route('POST /', array('RouteController', 'updateHome'));


/*
----------------------------------------------
SERVICES Page Routes
----------------------------------------------
*/
Flight::route('GET /services', function () {
    return renderPage('ServicesPage', Flight::get('ServicesPageID'), 'admin_services.php');
});
Flight::route('POST /services', array('RouteController', 'updateServices'));


/*
----------------------------------------------
PROJECT Routes
----------------------------------------------
*/
Flight::route('GET /project', array('ProjectController', 'renderHomePage')  );

// add new project
Flight::route('GET /project/add', array('ProjectController', 'renderAddPage')  );
Flight::route('POST /project/add', array('ProjectController', 'add')  );

// edit existing project
Flight::route('GET /project/edit/@id', array('ProjectController', 'renderEditPage')  );
Flight::route('POST /project/edit/@id', array('ProjectController', 'edit')  );

// delete existing project
Flight::route('POST /project/delete/@id', array('ProjectController', 'delete')  );


/*
----------------------------------------------
HISTORY Routes
----------------------------------------------
*/
Flight::route('GET /history', array('HistoryController', 'renderHomePage')  );

// add new history item
Flight::route('GET /history/add', array('HistoryController', 'renderAddPage')  );
Flight::route('POST /history/add', array('HistoryController', 'add')  );

// edit existing history item
Flight::route('GET /history/edit/@id', array('HistoryController', 'renderEditPage')  );
Flight::route('POST /history/edit/@id', array('HistoryController', 'edit')  );

// delete existing history
Flight::route('POST /history/delete/@id', array('HistoryController', 'delete')  );


/*
----------------------------------------------
BLOG Routes
----------------------------------------------
*/
Flight::route('GET /blog', array('BlogController', 'renderHomePage')  );
Flight::route('GET /blog/all', array('BlogController', 'generateFiles')  );

// add new blog post
Flight::route('GET /blog/add', array('BlogController', 'renderAddPage')  );
Flight::route('POST /blog/add', array('BlogController', 'add')  );

// edit existing blog post
Flight::route('GET /blog/edit/@id', array('BlogController', 'renderEditPage')  );
Flight::route('POST /blog/edit/@id', array('BlogController', 'edit')  );

// delete existing blog post
Flight::route('POST /blog/delete/@id', array('BlogController', 'delete')  );


/*
----------------------------------------------
login Routes
----------------------------------------------
*/
Flight::route('GET /login', function () {
  $current_user = ParseUser::getCurrentUser();
  if ($current_user) {
      return Flight::redirect('/');
  }

  return Flight::render('admin_login.php', array('path' => Flight::get('path') ));
});
Flight::route('POST /login', array('LoginController', 'login'));

/* Admin Logout Routes */
Flight::route('GET /logout', array('LoginController', 'logout'));


// Parse Error Handler
Flight::map('handleParseError', function($ex, $page){
  $code = $ex->getCode();
  switch($code) {
    // Invalid Session
    case 209:
      ParseUser::logOut();
      Flight::redirect('/login');
      break;
    // Username/Password Missing
    case 200:
    case 201:
      $error_message = "Please enter a username and password to login";
      break;
    // Operation Forbidden
    case 119:
      $error_message = "Only an Administrator can make changes to the database.";
      break;
    // Object not found
    case 101:
      $error_message = "The options you are trying to access could not be found. Please contact an admin for more help.";
      break;
    default:
      $error_message = "<strong>Error!</strong> Please reload the page to try again.";
  }

  $current_user = ParseUser::getCurrentUser();
  if($current_user) {
    return Flight::render($page, array('error' => $error_message, 'current_user' => ParseUser::getCurrentUser(), 'path' => Flight::get('path'), $options = '' ));
  } else {
      Flight::redirect('/login');
  }
});

/* Local Functions */
function renderPage($class, $class_object, $file)
{
    $query = new ParseQuery($class);
    $current_user = ParseUser::getCurrentUser();

    // User logged in, try to get page
    if($current_user) {
      try {
        $page_options = $query->get($class_object);
        return Flight::render($file, array('current_user' => ParseUser::getCurrentUser(), 'path' => Flight::get('path'), 'options' => $page_options));
      } catch (ParseException $ex) {
        return Flight::handleParseError($ex, $file);
      }

    // User not logged in so redirect to login page
    } else {
      return Flight::redirect('/login');
    }
}

// Sets the absolute path used for links
function setPaths() {
  $root = str_replace("\\", "/", __DIR__);
  $path = '';
  $frontend_path = $_SERVER['DOCUMENT_ROOT'];

  // set absolute path used for links
  if( strcmp($_SERVER['DOCUMENT_ROOT'], $root) !== 0 ) {
    $path = str_replace($_SERVER['DOCUMENT_ROOT'], '', $root);
    $path = ($path[0] !== '/' ? '/'.$path : $path);
    $frontend_path .= ( $frontend_path[strlen($frontend_path) - 1] === '/' ? 'frontend/' : '/frontend/');
  } else {
    // set frontend path used to generate html files
    $frontend_path .= ( $frontend_path[strlen($frontend_path) - 1] === '/' ? '../frontend/' : '/../frontend/');
  }

  Flight::set('path', $path);
  Flight::set('frontend_path', $frontend_path);
  Flight::set('blogmore_path', $frontend_path . 'moreblog/');
  Flight::set('upload_path', $frontend_path . 'images/');

  // Make frontend directory if it doesn't exist
  if( !is_dir(Flight::get('frontend_path')) ) {
    mkdir(Flight::get('frontend_path'));
  }

  if( !is_dir(Flight::get('blogmore_path')) ) {
    mkdir(Flight::get('blogmore_path'));
  }

}

Flight::start();
