<?php

use Parse\ParseObject;
use Parse\ParseUser;
use Parse\ParseException;
use Parse\ParseQuery;

class ProjectController
{
    // Deletes a history item with the specified id
    public static function delete($id)
    {
        $query = new ParseQuery('Project');
        try {
            $history = $query->get($id);
            $history->destroy();
            return Flight::redirect('/project');
        } catch (ParseException $ex) {
            return self::handleError($ex);
        }
    }

    // Edits the project with the specified id
    public static function edit($id)
    {
        $purifier = Flight::get('purifier');

        $project_title = !empty(Flight::request()->data->item_title) ? Flight::request()->data->item_title : "{missing-title}";
        $project_client = !empty(Flight::request()->data->item_client) ? Flight::request()->data->item_client : "{missing-client-name}";
        $project_duration = !empty(Flight::request()->data->item_duration) ? Flight::request()->data->item_duration : "{missing-project-duration}";
        $project_skills = !empty(Flight::request()->data->item_skills) ? Flight::request()->data->item_skills : "{missing-skills-list}";
        $project_description = !empty(Flight::request()->data->item_description) ? Flight::request()->data->item_description : "{missing-project-description}";
        $project_link = !empty(Flight::request()->data->item_project_link) ? Flight::request()->data->item_project_link : "index.php";
        $project_img_path = !empty(Flight::request()->data->item_img_path) ? Flight::request()->data->item_img_path : "{missing-image-path}";
        $query = new ParseQuery('Project');
        try {
            $project = $query->get($id);
            $project->set('title', $purifier->purify($project_title) );
            $project->set('client', $purifier->purify($project_client) );
            $project->set('duration', $purifier->purify($project_duration) );
            $project->set('skills', $purifier->purify($project_skills) );
            $project->set('description', $purifier->purify($project_description) );
            $project->set('project_link', $purifier->purify($project_link) );
            $project->set('img_path', $purifier->purify($project_img_path) );
            $project->save();
            return Flight::redirect('/project');
        } catch (ParseException $ex) {
            return self::handleError($ex);
        }
    }

    // Add a new history item
    public static function add()
    {
      $purifier = Flight::get('purifier');

      $project_title = !empty(Flight::request()->data->item_title) ? Flight::request()->data->item_title : "{missing-title}";
      $project_client = !empty(Flight::request()->data->item_client) ? Flight::request()->data->item_client : "{missing-client-name}";
      $project_duration = !empty(Flight::request()->data->item_duration) ? Flight::request()->data->item_duration : "{missing-project-duration}";
      $project_skills = !empty(Flight::request()->data->item_skills) ? Flight::request()->data->item_skills : "{missing-skills-list}";
      $project_description = !empty(Flight::request()->data->item_description) ? Flight::request()->data->item_description : "{missing-project-description}";
      $project_link = !empty(Flight::request()->data->item_project_link) ? Flight::request()->data->item_project_link : "index.php";
      $project_img_path = !empty(Flight::request()->data->item_img_path) ? Flight::request()->data->item_img_path : "{missing-image-path}";
      $query = new ParseQuery('Project');
      try {
          $project = new ParseObject("Project");
          $project->set('title', $purifier->purify($project_title) );
          $project->set('client', $purifier->purify($project_client) );
          $project->set('duration', $purifier->purify($project_duration) );
          $project->set('skills', $purifier->purify($project_skills) );
          $project->set('description', $purifier->purify($project_description) );
          $project->set('project_link', $purifier->purify($project_link) );
          $project->set('img_path', $purifier->purify($project_img_path) );
          $project->save();
          return Flight::redirect('/project');
      } catch (ParseException $ex) {
          return self::handleError($ex);
      }
    }

    /*
     * Methods used to render history related pages
     */
     public static function renderHomePage() {
         $query = new ParseQuery('Project');
         $current_user = ParseUser::getCurrentUser();

         if($current_user) {
           try {
               $projects = $query->find();
               return Flight::render('admin_project.php', array('current_user' => ParseUser::getCurrentUser(), 'path' => Flight::get('path'), 'projects' => $projects));
           } catch (ParseException $ex) {
                return Flight::handleParseError($ex, 'admin_project.php');
           }

         // User not logged in so redirect to login page
         } else {
           return Flight::redirect('/login');
         }
     }

    // Renders the page used to edit a history item
    public static function renderEditPage($id)
    {
        $query = new ParseQuery('Project');
        $current_user = ParseUser::getCurrentUser();

        if($current_user) {
          try {
              $item = $query->get($id);
              return Flight::render('admin_project_edit.php', array('current_user' => ParseUser::getCurrentUser(), 'path' => Flight::get('path'), 'item' => $item));
          } catch (ParseException $ex) {
              return self::handleError($ex);
          }

        // User not logged in so redirect to login page
        } else {
          return Flight::redirect('/login');
        }
    }

    // Renders the page used to add a new history item
    public static function renderAddPage()
    {
        $current_user = ParseUser::getCurrentUser();

        if($current_user) {
          return Flight::render('admin_project_add.php', array('current_user' => ParseUser::getCurrentUser(), 'path' => Flight::get('path') ));

        // User not logged in so redirect to login page
        } else {
          return Flight::redirect('/login');
        }
    }

    // Custom parse error handler for the history page
    public static function handleError($ex)
    {
      $code = $ex->getCode();

      // Invalid Session
      if($code === 209) {
          ParseUser::logOut();
          Flight::redirect('/login');
      }

      $current_user = ParseUser::getCurrentUser();
      if($current_user) {
          Flight::redirect('/project');
      } else {
          Flight::redirect('/login');
      }
    }

}
