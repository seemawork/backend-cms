# ParseObject Structure

### HomeOptions
###### Header Options
* title_1 => Main Title
* title_2 => Sub Title
* image_path => Background Image

###### Services Options
* icon => Font Awesome icon class
* title => Short phrase about the service
* paragraph => More detail about the service

###### Feedback Options
* person => Person's name
* paragraph => Feedback quote from the person

###### Figure Options
* number => Number of the figure
* stat => Name of the statistic for the figure

###### Example
```json
{
  "header_options": [
      {
          "title_1": "Welcome",
          "title_2": "to the world of tomorrow",
          "image_path": "images/image1.jpg"
      }
  ],
  "services_options": [
      {
          "icon": "fa-briefcase",
          "title": "Excellent Design",
          "paragraph": "Something... something... lorem ipsum"
      }
  ],
  "feedback_options": [
      {
          "person": "John Doe",
          "paragraph": "This website is the best thing since sliced bread!"
      }
  ],
  "figure_options": [
      {
          "number": "9000",
          "stat": "LOC Written"
      }
  ]
}
```


### ProjectOptions
###### Recent Projects
* title => Project Title
* client => Client Name
* duration => Time taken to complete the project
* description => Short description of the project
* image_path => Image showing off the project
* project_link => Link to the finished project

###### Other Projects
* title => Project Title
* image_path => Image showing off the project
* project_link => Link to the finished project

###### Example
```json
{
  "recent_projects": [
      {
          "title": "Project Name A",
          "client": "Jon Snow",
          "duration": "??????????",
          "description": "It was a project that happened",
          "image_path": "images/image1.jpg",
          "project_link": "http://paellaintelligence.com/"
      }
  ],
  "other_projects": [
      {
          "title": "Project Name Z",
          "image_path": "images/image2.jpg",
          "project_link": "http://paellaintelligence.com/"
      }
  ]
}
```


### BlogPost
* title => Post Title
* content => Post Content: Can contain html tags
* publish_date => Date when the post goes live  
  - Moment JS Format: `DD MMMM YYYY HH:mm`
  - PHP Date Format: `d F Y H:i`

###### Example
```json
[
    {
        "title": "Testing Things",
        "content": "<h1>Here is some content</h1>",
        "publish_date": "31 December 2016 00:00"
    }
]
  ```
