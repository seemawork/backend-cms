$(function () {
    var form_data = {};

    $("#send-data").click(function () {

         //Gets Services Options
            var services_options = [];
            $('.services-section').each(function () {
                var service = {};
                service.icon = $(this).find('#icon').val();
                service.title = $(this).find('#title').val();
                service.paragraph = $(this).find('#paragraph').val();
                services_options.push(service);
            });
            form_data.services_options = services_options;

        // Gets Testimonials
        var testimonials = [];
        $('.testimony').each(function () {
            var testimony = {};
            testimony.person = $(this).find('#person').val();
            testimony.quote = $(this).find('#quote').val();
            testimonials.push(testimony);
        });
        form_data.testimonials = testimonials;

        // Gets Figures
        var statistics = [];
        $('.statistic').each(function () {
            var figure = {};
            figure.number = $(this).find('#number').val();
            figure.stat = $(this).find('#stat').val();
            statistics.push(figure);
        });
        form_data.statistics = statistics;
        console.log(JSON.stringify(form_data));

        $('<input />').attr('type', 'hidden')
            .attr('name', "form_data")
            .attr('value', JSON.stringify(form_data))
            .appendTo('#home-form');

        $('#home-form').submit();
    });



});
