<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Paella Intelligence: Edit Home Page</title>
        <!-- Bootstrap minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <!-- Bootstrap Datepicker CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.css">
        <!-- Font Awesome CSS -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
        <!-- Roboto FONT -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic" rel="stylesheet" type="text/css">
        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Bootstrap minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <!-- Moment JS (dateformatter) -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
        <!-- Bootstrap Datepicker JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
        <!-- Template CSS -->
        <link rel="stylesheet" href="<?php echo $path . '/css/admin.css';?>">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>

    <body>

        <!-- Login Form START -->
        <section id="admin-main">
            <div class="container">
                <div class="row">

                  <!-- Navbar -->
                  <?php $history_active = "active"; ?>
                  <?php include 'partials/nav.php';?>

                    <div class="col-sm-9 section-header">

                        <h1>Add New History</h1>

                        <?php if (isset($error)) { ?>
                          <div id="error">
                              <h3><?php echo $error;?></h3>
                          </div>
                        <?php } else { ?>
                         <form method="POST" action="<?php echo $path . '/history/add';?>">

                           <!-- Date -->
                           <div class="form-group">
                             <label for="date-picker">Date</label>
                             <div class='input-group date' id='date-picker'>
                               <input type='text' name="item_date" class="form-control" placeholder="Date" />
                               <span class="input-group-addon">
                                 <span class="glyphicon glyphicon-calendar"></span>
                               </span>
                             </div>
                           </div>


                           <div class="form-group">
                               <label for="item-icon">Icon</label>
                               <input type="text" class="form-control" id="item-title" name="item_icon" placeholder="Font Awesome Icon" >
                           </div>

                           <div class="form-group reset">
                               <label for="item-content">Content</label>
                               <textarea class="form-control" id="item-content" name="item_content" placeholder="A small description about a historic event at the company"></textarea>
                           </div>
                           <button type="submit" class="btn btn-default">Save</button>
                         </form>
                         <script>
                           $(document).ready(function() {
                              $('#date-picker').datetimepicker({
                                format: 'DD MMMM YYYY'
                              });
                           });
                         </script>

                        <?php } ?>

                    </div>
                </div>
                <!-- ./row -->
            </div>
            <!-- /.container -->
        </section>

    </body>

</html>
