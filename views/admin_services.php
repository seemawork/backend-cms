<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Paella Intelligence: Edit Home Page</title>
    <!-- Bootstrap minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Roboto FONT -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic" rel="stylesheet" type="text/css">
    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Bootstrap minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <!-- Template CSS -->
    <link rel="stylesheet" href="<?php echo $path . '/css/admin.css';?>">
    <!-- JS -->
    <script src="<?php echo $path . '/js/admin_services.js';?>"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <!-- Login Form START -->
    <section id="admin-main">
        <div class="container">
            <div class="row">

              <!-- Navbar -->
              <?php $services_active = "active"; ?>
              <?php include 'partials/nav.php';?>

                <div class="col-sm-9 section-header">
                    <h1>Edit Services Page</h1>
                    <?php if (isset($error)) { ?>
                        <div id="error">
                            <h3><?php echo $error;?></h3> </div>
                        <?php } else { ?>
                            <div class="form-horizontal">
                                <!--SUB TITLE OPTIONS START -->
                                <h4 class="option-header">Service Options</h4>
                                <?php
                                $services_options = $options->get('services_options');
                                //$length = max(6, count($services_options));
                                for ($i = 0; $i < 6; ++$i) {
                                    $title = '';
                                    $para = '';
                                    $icon ='';


                                    if (isset($services_options[$i])) {
                                        $title = isset($services_options[$i]['title']) ? $services_options[$i]['title'] : '';
                                        $para = isset($services_options[$i]['para']) ? $services_options[$i]['para'] : '';
                                        $icon = isset($services_options[$i]['icon']) ? $services_options[$i]['icon'] : '';
                                           }
                              ?>
                                    <div class="sub_title">
                                        <!-- Title One -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                <?php echo 'Title '.($i+1)?>
                                            </label>
                                            <div class="col-sm-8">
                                                <input type="text" id="title" class="form-control" placeholder="Title 1" value="<?php echo $title;?>"> </div>
                                        </div>
                                        <!-- Para one -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?php echo 'Para '.($i+1);?></label>
                                            <div class="col-sm-8">
                                                <textarea rows="3" id="para" class="form-control" placeholder="para"><?php echo $para;?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                <?php echo 'icon '.($i+1);?>
                                            </label>
                                            <div class="col-sm-8">
                                                <input type="text" id="icon" class="form-control" placeholder="Font Awesome Icon Name" value="<?php echo $icon;?>"> </div>
                                        </div>
                                    </div>
                                    <?php } // For Loop END ?>
                                        <!--  OPTIONS END -->
                            </div>
                            
                            <!-- Submit Form -->
                            <form id="home-form" method="POST" action="<?php echo $path . '/services';?>">
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-10">
                                        <button type="button" id="send-data" value="submit" class="button-primary btn btn-default">Send Data</button>
                                    </div>
                                </div>
                            </form>
                            <?php } ?>
                </div>
            </div>
            <!-- ./row -->
        </div>
        <!-- /.container -->
    </section>
</body>

</html>
