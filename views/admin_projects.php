<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Paella Intelligence: Edit Home Page</title>
        <!-- Bootstrap minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <!-- Roboto FONT -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic" rel="stylesheet" type="text/css">
        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Bootstrap minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <!-- Template CSS -->
        <link rel="stylesheet" href="<?php echo $path . '/css/admin.css';?>">
        <!-- JS -->
        <script src="<?php echo $path . '/js/admin_projects.js';?>"></script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>

    <body>

        <!-- Login Form START -->
        <section id="admin-main">
            <div class="container">
                <div class="row">

                  <!-- Navbar -->
                  <?php $projects_active = "active"; ?>
                  <?php include 'partials/nav.php';?>

                    <div class="col-sm-9 section-header">

                        <h1>Edit Projects Page</h1>

                        <?php if (isset($error)) { ?>
                        <div id="error">
                            <h3><?php echo $error;?></h3>
                        </div>


                        <?php } else { ?>
                        <div class="form-horizontal">

                            <!-- RECENT PROJECTS START -->
                            <h4 class="option-header">Recent Projects Options</h4>
                            <?php
                                $recent_projects = $options->get('recent_projects');
                                $length = max(2, count($recent_projects));
                                for ($i = 0; $i < $length; ++$i) {
                                    $title = ''; $client = ''; $duration = ''; $description = ''; $image_path = ''; $project_link = '';

                                    if (isset($recent_projects[$i])) {
                                        $title = isset($recent_projects[$i]['title']) ? $recent_projects[$i]['title'] : '';
                                        $client = isset($recent_projects[$i]['client']) ? $recent_projects[$i]['client'] : '';
                                        $duration = isset($recent_projects[$i]['duration']) ? $recent_projects[$i]['duration'] : '';
                                        $description = isset($recent_projects[$i]['description']) ? $recent_projects[$i]['description'] : '';
                                        $image_path = isset($recent_projects[$i]['image_path']) ? $recent_projects[$i]['image_path'] : '';
                                        $project_link = isset($recent_projects[$i]['project_link']) ? $recent_projects[$i]['project_link'] : '';
                                    }
                              ?>
                                <div class="recent-project">
                                  <div class="form-group">
                                      <h4 class="col-sm-3 control-label title"><?php echo 'Section ' . $i ?></h4>
                                  </div>

                                    <!-- Title -->
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            <?php echo 'Title'?>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text" id="title" class="form-control" placeholder="Title" value="<?php echo htmlspecialchars($title);?>">
                                        </div>
                                    </div>

                                    <!-- Client -->
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            <?php echo 'Client'?>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text" id="client" class="form-control" placeholder="Client Name" value="<?php echo htmlspecialchars($client);?>">
                                        </div>
                                    </div>

                                    <!-- Duration -->
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            <?php echo 'Duration'?>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text" id="duration" class="form-control" placeholder="Project Duration" value="<?php echo htmlspecialchars($duration);?>">
                                        </div>
                                    </div>

                                    <!-- Description -->
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            <?php echo 'Description';?>
                                        </label>
                                        <div class="col-sm-8">
                                            <textarea rows="2" id="description" class="form-control" placeholder="Brief Descrition of the project"><?php echo htmlspecialchars($description);?></textarea>
                                        </div>
                                    </div>

                                    <!-- Image Path -->
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            <?php echo 'Image Path'?>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text" id="image_path" class="form-control" placeholder="Image Path" value="<?php echo htmlspecialchars($image_path);?>">
                                        </div>
                                    </div>

                                    <!-- Project Link -->
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            <?php echo 'Project Link'?>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text" id="project_link" class="form-control" placeholder="Link to the project" value="<?php echo htmlspecialchars($project_link);?>">
                                        </div>
                                    </div>
                                </div>
                                <?php
                                  } // For Loop END
                                ?>
                                <!-- RECENT PROJECTS END -->

                                <!-- OTHER PROJECTS START -->
                                <h4 class="option-header">Other Projects Options</h4>
                                <?php
                                    $other_projects = $options->get('other_projects');
                                    $length = max(3, count($other_projects));

                                    for ($i = 0; $i < $length; ++$i) {
                                        $title = ''; $image_path = ''; $project_link = '';

                                        if (isset($other_projects[$i])) {
                                            $title = isset($other_projects[$i]['title']) ? $other_projects[$i]['title'] : '';
                                            $image_path = isset($other_projects[$i]['image_path']) ? $other_projects[$i]['image_path'] : '';
                                            $project_link = isset($other_projects[$i]['project_link']) ? $other_projects[$i]['project_link'] : '';
                                        }
                                ?>
                                    <div class="other-project">
                                        <div class="form-group">
                                            <h4 class="col-sm-3 control-label title"><?php echo 'Section ' . $i ?></h4>
                                        </div>

                                        <!-- Title -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                <?php echo 'Title';?>
                                            </label>
                                            <div class="col-sm-8">
                                                <input type="text" id="title" class="form-control" placeholder="Title" value="<?php echo htmlspecialchars($title);?>">
                                            </div>
                                        </div>

                                        <!-- Image Path -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                <?php echo 'Image Path'?>
                                            </label>
                                            <div class="col-sm-8">
                                                <input type="text" id="image_path" class="form-control" placeholder="Image Path" value="<?php echo htmlspecialchars($image_path);?>">
                                            </div>
                                        </div>

                                        <!-- Project Link -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                <?php echo 'Project Link'?>
                                            </label>
                                            <div class="col-sm-8">
                                                <input type="text" id="project_link" class="form-control" placeholder="Link to the project" value="<?php echo htmlspecialchars($project_link);?>">
                                            </div>
                                        </div>

                                    </div>
                                <?php } // For Loop END ?>
                                <!-- OTHER PROJECTS END -->


                                <!-- Submit Form -->
                                <form id="form" method="POST" action="<?php echo $path . '/projects';?>">
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-10">
                                            <button type="button" id="send-data" value="submit" class="button-primary btn btn-default">Send Data</button>
                                        </div>
                                    </div>
                                </form>
                        </div>
                        <?php } ?>

                    </div>
                </div>
                <!-- ./row -->
            </div>
            <!-- /.container -->
        </section>

    </body>

</html>
