$(function () {
    // Gets Section Options
    var form_data = {};
    $("#send-data").click(function () {
        // Gets Header Options
        var section_options = [];
        $('.section_options').each(function () {
            var section = {};
            section.paragraph = $(this).find('#paragraph').val();
            section.image = $(this).find('#image').val();
            section_options.push(section);
        });
        form_data.section_options = section_options;
        // Gets team Options
        var team_options = [];
        $('.team_options').each(function () {
            var options = {};
            options.person_name = $(this).find('#person_name').val();
            options.job_title = $(this).find('#job_title').val();
            options.image = $(this).find('#image').val();
            team_options.push(options);
        });
        form_data.team_options = team_options;
        $('<input />').attr('type', 'hidden').attr('name', "form_data").attr('value', JSON.stringify(form_data)).appendTo('#home-form');
        $('#home-form').submit();
    });
});