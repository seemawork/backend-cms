$(function () {
    var form_data = {};

    $(".delete").click(function () {
        var delete_post = confirm("Title: " + $(this).data("title") + "\nAre you sure you want to delete this item?");
        if (delete_post === true) {
            $('#form').attr('action', $(this).data('link'));
            $('#form').submit();
        }
    });

});
