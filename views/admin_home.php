<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Paella Intelligence: Edit Home Page</title>
        <!-- Bootstrap minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <!-- Roboto FONT -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic" rel="stylesheet" type="text/css">
        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Bootstrap minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <!-- Template CSS -->
        <link rel="stylesheet" href="<?php echo $path . '/css/admin.css';?>">
        <!-- JS -->
        <script src="<?php echo $path . '/js/admin_home.js';?>"></script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>

    <body>

        <!-- Login Form START -->
        <section id="admin-main">
            <div class="container">
                <div class="row">

                  <!-- Navbar -->
                  <?php $home_active = "active"; ?>
                  <?php include 'partials/nav.php';?>

                    <div class="col-sm-9 section-header">

                        <h1>Edit Home Page</h1>

                        <?php if (isset($error)) { ?>
                        <div id="error">
                            <h3><?php echo $error;?></h3>
                        </div>


                        <?php } else { ?>
                        <div class="form-horizontal">

                                <!-- SERVICES OPTIONS START -->
                                <h4 class="option-header">Our Services Options</h4>
                                <?php
                                    $services_options = $options->get('services_options');
                                  //  $length = max(2, count($services_options));

                                    for ($i = 0; $i < 3; ++$i) {
                                        $icon = '';
                                        $title = '';
                                        $paragraph = '';

                                        if (isset($services_options[$i])) {
                                            $icon = isset($services_options[$i]['icon']) ? $services_options[$i]['icon'] : '';
                                            $title = isset($services_options[$i]['title']) ? $services_options[$i]['title'] : '';
                                            $paragraph = isset($services_options[$i]['paragraph']) ? $services_options[$i]['paragraph'] : '';
                                        }
                                ?>
                                    <div class="services-section">
                                      <div class="form-group">
                                          <h4 class="col-sm-3 control-label title"><?php echo 'Section ' . $i ?></h4>
                                      </div>

                                        <!-- Icon  -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                <?php echo 'Icon';?>
                                            </label>
                                            <div class="col-sm-8">
                                                <input type="text" id="icon" class="form-control" placeholder="Font Awesome Icon Name" value="<?php echo htmlspecialchars($icon);?>">
                                            </div>
                                        </div>

                                        <!-- Title -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                <?php echo 'Title';?>
                                            </label>
                                            <div class="col-sm-8">
                                                <input type="text" id="title" class="form-control" placeholder="Title" value="<?php echo htmlspecialchars($title);?>">
                                            </div>
                                        </div>

                                        <!-- Paragraph -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                <?php echo 'paragraph';?>
                                            </label>
                                            <div class="col-sm-8">
                                                <textarea rows="2" id="paragraph" class="form-control" placeholder="Paragraph"><?php echo htmlspecialchars($paragraph);?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                <?php } // For Loop END ?>
                                <!-- SERVICES OPTIONS END -->

                                <!-- TESTIMONIALS START -->
                                <h4 class="option-header">Testimonials</h4>
                                <?php
                                    $testimonials = $options->get('testimonials');
                                    $length = max(3, count($testimonials));

                                    for ($i = 0; $i < $length; ++$i) {
                                        $person = '';
                                        $quote = '';

                                        if (isset($testimonials[$i])) {
                                            $person = isset($testimonials[$i]['person']) ? $testimonials[$i]['person'] : '';
                                            $quote = isset($testimonials[$i]['quote']) ? $testimonials[$i]['quote'] : '';
                                        }
                                ?>
                                    <div class="testimony">
                                        <div class="form-group">
                                            <h4 class="col-sm-3 control-label title"><?php echo 'Testimony ' . ($i+1) ?></h4>
                                        </div>

                                        <!-- Person -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                <?php echo 'Person Name';?>
                                            </label>
                                            <div class="col-sm-8">
                                                <input type="text" id="person" class="form-control" placeholder="Person Name" value="<?php echo htmlspecialchars($person);?>">
                                            </div>
                                        </div>

                                        <!-- Quote -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                <?php echo 'Quote';?>
                                            </label>
                                            <div class="col-sm-8">
                                              <textarea rows="2" id="quote" class="form-control" placeholder="Quote"><?php echo htmlspecialchars($quote);?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } // For Loop END ?>
                                        <!-- TESTIMONIALS END -->

                                    <!-- STATISTICS START -->
                                    <h4 class="option-header">Statistics</h4>
                                    <?php
                                        $statistics = $options->get('statistics');
                                        $length = max(4, count($statistics));


                                            for ($i = 0; $i < $length; ++$i) {
                                                $number = '';
                                                $stat = '';

                                                if (isset($statistics[$i])) {
                                                    $number = isset($statistics[$i]['number']) ? $statistics[$i]['number'] : '';
                                                    $stat = isset($statistics[$i]['stat']) ? $statistics[$i]['stat'] : '';
                                                }
                                    ?>
                                        <div class="statistic">
                                          <div class="form-group">
                                              <h4 class="col-sm-3 control-label title"><?php echo 'Statistic ' . ($i+1) ?></h4>
                                          </div>

                                            <!-- Number -->
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">
                                                    <?php echo 'Number';?>
                                                </label>
                                                <div class="col-sm-8">
                                                    <input type="number" id="number" class="form-control" placeholder="Figure Number" value="<?php echo htmlspecialchars($number)?>">
                                                </div>
                                            </div>

                                            <!-- Statistic -->
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">
                                                    <?php echo 'Statistic';?>
                                                </label>
                                                <div class="col-sm-8">
                                                    <input type="text" id="stat" class="form-control" placeholder="Statistic Name" value="<?php echo htmlspecialchars($stat)?>">
                                                </div>
                                            </div>

                                        </div>
                                    <?php } // For Loop END ?>
                                    <!-- STATISTICS END -->


                                    <!-- Submit Form -->
                                    <form id="home-form" method="POST" action="<?php echo $path . '/';?>">
                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-10">
                                                <button type="button" id="send-data" value="submit" class="button-primary btn btn-default">Send Data</button>
                                            </div>
                                        </div>
                                    </form>
                        </div>
                        <?php } ?>

                    </div>
                </div>
                <!-- ./row -->
            </div>
            <!-- /.container -->
        </section>

    </body>

</html>
