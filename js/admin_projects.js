$(function () {
    var form_data = {};

    $("#send-data").click(function () {

        // Gets Recent Projects
        var recent_projects = [];
        $('.recent-project').each(function () {
            var project = {};
            project.title = $(this).find('#title').val();
            project.client = $(this).find('#client').val();
            project.duration = $(this).find('#duration').val();
            project.description = $(this).find('#description').val();
            project.image_path = $(this).find('#image_path').val();
            project.project_link = $(this).find('#project_link').val();
            recent_projects.push(project);
        });
        form_data.recent_projects = recent_projects;

        // Get Other Projects
        var other_projects = [];
        $('.other-project').each(function () {
            var project = {};
            project.title = $(this).find('#title').val();
            project.image_path = $(this).find('#image_path').val();
            project.project_link = $(this).find('#project_link').val();
            other_projects.push(project);
        });
        form_data.other_projects = other_projects;

        $('<input />').attr('type', 'hidden')
            .attr('name', "form_data")
            .attr('value', JSON.stringify(form_data))
            .appendTo('#form');

        $('#form').submit();
    });



});
