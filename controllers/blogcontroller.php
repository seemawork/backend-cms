<?php

use Parse\ParseObject;
use Parse\ParseUser;
use Parse\ParseException;
use Parse\ParseQuery;

class BlogController
{
    // Deletes a blog post with the specified id
    public static function delete($id)
    {
        $query = new ParseQuery('BlogPost');
        try {
            $post = $query->get($id);
            $file = Flight::get('blogmore_path') . $id . '.html';
            $post->destroy();

            // Delete html file generated on creation
            if(file_exists($file) ) {
              unlink($file);
            }

            return Flight::redirect('/blog');
        } catch (ParseException $ex) {
            return self::handleError($ex);
        }
    }

    // Edits the blog post with the specified id
    public static function edit($id)
    {
        static $error_message = '<strong>Error!</strong> Please reload the page to try again.';
        $purifier = Flight::get('purifier');

        $post_title = !empty(Flight::request()->data->post_title) ? Flight::request()->data->post_title : "{missing-title}";
        $post_description = !empty(Flight::request()->data->post_description) ? Flight::request()->data->post_description : "{missing-description}";
        $publish_date = !empty(Flight::request()->data->publish_date) ? Flight::request()->data->publish_date :  date('d F Y');
        $post_name = !empty(Flight::request()->data->post_name) ? Flight::request()->data->post_name : "{missing-name}";


        $query = new ParseQuery('BlogPost');
        try {
            $post = $query->get($id);
            $post_image = self::setBlogImage( $post->get('image') );
           $post->set('title', $purifier->purify($post_title) );
            $post->set('description', $purifier->purify($post_description) );
            $post->set('publish_date', $purifier->purify($publish_date) );
             $post->set('name', $purifier->purify($post_name) );
           $post->set('image', $purifier->purify($post_image) );
            $post_link = $id.'.html' ;
            $post->set('link', $purifier->purify($post_link) );

            $post->save();

           self::generateHtml($post);
            return Flight::redirect('/blog');

        } catch (ParseException $ex) {
            return self::handleError($ex);
        }
    }

    // Add a new blog post
    public static function add()
    {
        static $error_message = '<strong>Error!</strong> Please reload the page to try again.';
        $purifier = Flight::get('purifier');

        $post_title = !empty(Flight::request()->data->post_title) ? Flight::request()->data->post_title : "{missing-title}";
        $post_description = !empty(Flight::request()->data->post_description) ? Flight::request()->data->post_description : "{missing-fgfgfgfgdescription}";
        $publish_date = !empty(Flight::request()->data->publish_date) ? Flight::request()->data->publish_date :  date('d F Y');
        $post_name = !empty(Flight::request()->data->post_name) ? Flight::request()->data->post_name : "{missing-name}";
        $post_image = self::setBlogImage();


       try {
            $post = new ParseObject("BlogPost");
            $post->set('title', $purifier->purify($post_title) );
            $post->set('description', $purifier->purify($post_description) );
            $post->set('publish_date', $purifier->purify($publish_date) );
            $post->set('name', $purifier->purify($post_name));
            $post->set('image', $purifier->purify($post_image));
             $post->save();
              $post->set('link', $post->getObjectId().'.html' );
             $post->save();
            self::generateHtml($post);
            return Flight::redirect('/blog');
        } catch (ParseException $ex) {
            return self::handleError($ex);
        }

    }

    public static function setBlogImage($old = NULL) {
        $result = !empty($old) ? $old : '/images/placeholder.png';

        // Successful Upload Check
        if( $_FILES['file_uploaded']['error'] === UPLOAD_ERR_OK ) {
            $allowed_types = array("jpg", "jpeg", "png", "gif");
            $target_file = Flight::get('upload_path') . basename($_FILES["file_uploaded"]["name"]);
            $file_type = pathinfo($target_file,PATHINFO_EXTENSION);
            $check = getimagesize($_FILES["file_uploaded"]["tmp_name"]);

            // Valid Image Check
            if($check !== false && !file_exists($target_file) && in_array($file_type, $allowed_types) ) {
                if(move_uploaded_file($_FILES["file_uploaded"]["tmp_name"], $target_file) ) {
                    $result = '/images/' . basename($_FILES["file_uploaded"]["name"]);
                }
            }
        }
       return $result;
    }

    /*
     * Methods used to render blog related pages
     */
     public static function renderHomePage() {
         $query = new ParseQuery('BlogPost');
         $current_user = ParseUser::getCurrentUser();

         if($current_user) {
           try {
               $posts = $query->find();
               return Flight::render('admin_blog.php', array('current_user' => ParseUser::getCurrentUser(), 'path' => Flight::get('path'), 'posts' => $posts));
           } catch (ParseException $ex) {
                return Flight::handleParseError($ex, 'admin_blog.php');
           }

         // User not logged in so redirect to login page
         } else {
           return Flight::redirect('/login');
         }
     }

    // Renders the page used to edit a blog post
    public static function renderEditPage($id)
    {
        $query = new ParseQuery('BlogPost');
        $current_user = ParseUser::getCurrentUser();

        if($current_user) {
          try {
              $post = $query->get($id);
              return Flight::render('admin_blog_edit.php', array('current_user' => ParseUser::getCurrentUser(), 'path' => Flight::get('path'), 'post' => $post));
          } catch (ParseException $ex) {
              return self::handleError($ex);
          }

        // User not logged in so redirect to login page
        } else {
          return Flight::redirect('/login');
        }
    }

    // Renders the page used to add a new blog post
    public static function renderAddPage()
    {
        $current_user = ParseUser::getCurrentUser();

        if($current_user) {
          return Flight::render('admin_blog_add.php', array('current_user' => ParseUser::getCurrentUser(), 'path' => Flight::get('path') ));

        // User not logged in so redirect to login page
        } else {
          return Flight::redirect('/login');
        }
    }

    // Custom parse error handler for the blog page
    public static function handleError($ex)
    {
      $code = $ex->getCode();

      // Invalid Session
      if($code === 209) {
          ParseUser::logOut();
          Flight::redirect('/login');
      }

      $current_user = ParseUser::getCurrentUser();
      if($current_user) {
          Flight::redirect('/blog');
      } else {
          Flight::redirect('/login');
      }
    }

    /* Quickly generate all the blog post html files */
    public static function generateFiles() {
      $query = new ParseQuery('BlogPost');
      try {
          $posts = $query->find();
          foreach($posts as $post) {
            self::generateHtml($post);
          }
          return Flight::redirect('/blog');
      } catch (ParseException $ex) {
          return self::handleError($ex);
      }
    }

    private static function generateHtml($parse_object)
    {


         $parse_object->set('link', $parse_object->getObjectId().'.html' );

        ob_start(); // start output buffer
        $template_file = 'blogmore.php';
        $blog_file_name = $parse_object->getObjectId() . '.html';

       $options = $parse_object;
        include $template_file;
        $template = ob_get_contents(); // get contents of buffer
        ob_end_clean();
        file_put_contents( Flight::get('blogmore_path') . $blog_file_name, $template);
    }

}
