<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Paella Intelligence: Edit Home Page</title>
        <!-- Bootstrap minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <!-- Bootstrap Datepicker CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.css">
        <!-- Font Awesome CSS -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
        <!-- Roboto FONT -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic" rel="stylesheet" type="text/css">
        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Bootstrap minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <!-- Moment JS (dateformatter) -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
        <!-- Bootstrap Datepicker JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
        <!-- Template CSS -->
        <link rel="stylesheet" href="<?php echo $path . '/css/admin.css';?>">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>

    <body>

        <!-- Login Form START -->
        <section id="admin-main">
            <div class="container">
                <div class="row">

                  <!-- Navbar -->
                  <?php $project_active = "active"; ?>
                  <?php include 'partials/nav.php';?>

                    <div class="col-sm-9 section-header">

                        <h1>Edit Project</h1>

                        <?php if (isset($error)) { ?>
                          <div id="error">
                              <h3><?php echo $error;?></h3>
                          </div>
                        <?php } else { ?>
                         <form method="POST" action="<?php echo $path . '/project/edit/' . $item->getObjectId();?>">

                           <!-- Title -->
                           <div class="form-group">
                               <label for="item-title">Project Title</label>
                               <input type="text" class="form-control" id="item-title" name="item_title" placeholder="Project Title" value="<?php echo htmlspecialchars($item->get('title') ); ?>" >
                           </div>

                           <!-- Client -->
                           <div class="form-group">
                               <label for="item-client">Project's Client</label>
                               <input type="text" class="form-control" id="item-client" name="item_client" placeholder="Project's Client" value="<?php echo htmlspecialchars($item->get('client') ); ?>" >
                           </div>

                           <!-- Duration -->
                           <div class="form-group">
                               <label for="item-duration">Project Duration</label>
                               <input type="text" class="form-control" id="item-duration" name="item_duration" placeholder="Project's Client" value="<?php echo htmlspecialchars($item->get('duration') ); ?>" >
                           </div>

                           <!-- Skills -->
                           <div class="form-group">
                               <label for="item-skills">Skills Used</label>
                               <input type="text" class="form-control" id="item-skills" name="item_skills" placeholder="List of Skills e.g HTML, CSS etc." value="<?php echo htmlspecialchars($item->get('skills') ); ?>" >
                           </div>

                           <!-- Description -->
                           <div class="form-group reset">
                               <label for="item-description">Project Description</label>
                               <textarea class="form-control" id="item-description" name="item_description" placeholder="A small desciption of the project"><?php echo htmlspecialchars($item->get('description') ); ?></textarea>
                           </div>

                           <!-- Skills -->
                           <div class="form-group">
                               <label for="item-link">Project Link</label>
                               <input type="text" class="form-control" id="item-link" name="item_project_link" placeholder="A link to the finished project" value="<?php echo htmlspecialchars($item->get('project_link') ); ?>" >
                           </div>

                           <!-- Image Path -->
                           <div class="form-group">
                               <label for="item-path">Project Image</label>
                               <input type="text" class="form-control" id="item-path" name="item_img_path" placeholder="The path of the image showing off the project e.g images/test.png" value="<?php echo htmlspecialchars($item->get('img_path') ); ?>" >
                           </div>

                           <button type="submit" class="btn btn-default">Save</button>
                         </form>
                        <?php } ?>

                    </div>
                </div>
                <!-- ./row -->
            </div>
            <!-- /.container -->
        </section>

    </body>

</html>
